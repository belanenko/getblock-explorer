include .env 
-include .env.local
export 

.DEFAULT_GOAL := up

clearscreen:
	clear
.PHONY: clearscreen


lint: clearscreen
	golangci-lint run -v ./...
.PHONY: lint

upd: lint down
	docker-compose up --build -d
.PHONY: up

up: lint down
	docker-compose up --build
.PHONY: up

down:
	docker-compose down
.PHONY: down

build:
	go build -o build/eth-explorer ./cmd/eth-explorer/
.PHONY: build

run-local: build	
	./build/eth-explorer
.PHONY: run-local


