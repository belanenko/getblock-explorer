# eth-explorer

## How can start explorer?

In first you must add ethereum node url env `ETH_NODE_URL` to .env or .env.local file.

Configuration eximple file `.env`:

```env
EXPLORER_PORT=8080

DB_USER=postgres
DB_PASSWORD=postgres
DB_HOST=db
DB_PORT=5432

ETH_NODE_URL=wss://eth.getblock.io/<API-KEY>/mainnet/
BLOCKS_SYNC_COUNT=100
SYNC_RATE_LIMIT=60

LOG_LEVEL=info
```

Aftef configuration you can start explorer in docker:

```bash
make
```

or you can build project in binary file:

```bash
make build
``` 

you can find binary file in `/build`

---

## Usage exapmple:

```bash 
GET http://127.0.0.1:8080/address

response: 200 OK
{"address":"0xAAB27b150451726EC7738aa1d0A94505c8729bd1"}