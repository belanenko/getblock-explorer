package config

import (
	"gitlab.com/belanenko/getblock-explorer/internal/api"
	"gitlab.com/belanenko/getblock-explorer/internal/db"
	"gitlab.com/belanenko/getblock-explorer/internal/logger"
	"gitlab.com/belanenko/getblock-explorer/internal/node"

	"github.com/caarlos0/env/v6"
	"github.com/pkg/errors"
)

type Config struct {
	Logger logger.Config
	DB     db.Config
	Node   node.Config
	API    api.Config
}

func ParseEnv() (*Config, error) {
	var cfg Config
	if err := env.Parse(&cfg); err != nil {
		return nil, errors.Wrap(err, "can't parse env")
	}
	return &cfg, nil
}

func (c *Config) Validate() error {
	if err := c.Logger.Validate(); err != nil {
		return err
	}
	if err := c.DB.Validate(); err != nil {
		return err
	}
	if err := c.Node.Validate(); err != nil {
		return err
	}
	if err := c.API.Validate(); err != nil {
		return err
	}
	return nil
}
