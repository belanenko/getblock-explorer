FROM golang:1.19.3-alpine AS builder
WORKDIR /build
RUN apk add --no-cache gcc musl-dev
COPY ./go.mod ./
RUN go mod download
COPY ./ ./
RUN go build -o build/eth-explorer ./cmd/eth-explorer/

FROM alpine:3.11.3
WORKDIR /eth-explorer
RUN apk --no-cache add ca-certificates
COPY --from=builder  /build/build/eth-explorer .
ENTRYPOINT [ "./eth-explorer" ]
 