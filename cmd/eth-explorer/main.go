package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/belanenko/getblock-explorer/config"
	"gitlab.com/belanenko/getblock-explorer/internal/api"
	storage "gitlab.com/belanenko/getblock-explorer/internal/db"
	"gitlab.com/belanenko/getblock-explorer/internal/logger"
	"gitlab.com/belanenko/getblock-explorer/internal/node"
	"gitlab.com/belanenko/getblock-explorer/migrations"

	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

func main() {
	cfg, err := config.ParseEnv()
	if err != nil {
		logrus.WithError(err).Fatal("can't parse config")
	}
	l, err := logger.New(&cfg.Logger)
	if err != nil {
		l.WithError(err).Fatal("create logger")
	}

	db, err := storage.New(&cfg.DB, l)
	if err != nil {
		l.WithError(err).Fatal("can't create pool")
	}

	m := migrations.New(cfg.DB.String(), l)

	if err = m.Up(); err != nil {
		l.WithError(err).Fatal("can't execute migrations")
	}

	n, err := node.New(&cfg.Node, db, l)
	if err != nil {
		l.WithError(err).Fatal("can't create node")
	}

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err = n.Sync(ctx); err != nil {
			if !errors.Is(err, context.Canceled) {
				l.WithError(err).Fatal("can't sync node and db")
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		a := api.New(&cfg.API, db, l)
		if err := a.Start(ctx); err != nil {
			if !errors.Is(http.ErrServerClosed, err) {
				l.WithError(err).Fatal("start api")
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := n.Subscribe(ctx); err != nil {
			if !errors.Is(err, context.Canceled) {
				l.WithError(err).Fatal("Subscribe()")
			}
		}
	}()

	<-ctx.Done()
	l.Info("Start shutdowning...")
	cancel()

	wg.Wait()
	l.Info("all is ok!")
	os.Exit(0)
}
