package node

import (
	"context"
	"math/big"
	"sync"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	geth "github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"gitlab.com/belanenko/getblock-explorer/internal/db"
	"gitlab.com/belanenko/getblock-explorer/internal/logger"
	"golang.org/x/time/rate"
)

type Node struct {
	client      *geth.Client
	storage     *db.DB
	logger      *logger.Logger
	cfg         *Config
	rateLimiter *rate.Limiter
}

func New(cfg *Config, storage *db.DB, l *logger.Logger) (*Node, error) {
	client, err := geth.Dial(cfg.URL)
	if err != nil {
		l.WithError(err).Error("dial node")
		return nil, err
	}

	node := &Node{
		cfg:         cfg,
		client:      client,
		storage:     storage,
		logger:      l,
		rateLimiter: rate.NewLimiter(rate.Every(time.Second), cfg.RateLimit),
	}
	return node, nil
}

func (n *Node) Sync(ctx context.Context) error {
	dbBlock, err := n.storage.BlockNumber(ctx)
	if err != nil {
		n.logger.WithError(err).Error("get block number from storage")
		return err
	}
	var nodeBlock uint64
Loop:
	for {
		select {
		case <-ctx.Done():
			return nil
		default:
			nodeBlock, err = n.client.BlockNumber(ctx)
			if err != nil {
				n.logger.WithError(err).Error("can't get block number from node")
				continue
			}
			break Loop
		}
	}

	bc := nodeBlock - n.cfg.CountSyncBlocks
	if dbBlock < bc {
		dbBlock = bc
	}

	blockCh := make(chan *types.Block, n.cfg.CountSyncBlocks)
	wg := sync.WaitGroup{}
	for i := dbBlock; i <= nodeBlock; i++ {
		wg.Add(1)
		go func(num int64) {
			defer wg.Done()
		Loop:
			for {
				select {
				case <-ctx.Done():
					return
				default:
					if errLimiter := n.rateLimiter.Wait(ctx); errLimiter != nil {
						n.logger.WithError(errLimiter).Error("rate limit")
						return
					}
					block, errGetBlock := n.client.BlockByNumber(ctx, big.NewInt(num))
					if errGetBlock != nil {
						n.logger.WithError(errGetBlock).Error("can't get BlockByNumber from node")
						continue
					}
					blockCh <- block
					break Loop
				}
			}
		}(int64(i))
	}

	blocks := []*types.Block{}

	go func() {
		for block := range blockCh {
			blocks = append(blocks, block)
		}
	}()
	wg.Wait()
	close(blockCh)

	select {
	case <-ctx.Done():
		return nil
	default:
	}

	err = n.storage.AddBlocks(ctx, blocks...)
	if err != nil {
		return errors.Wrap(err, "can't add blocks to db")
	}
	return nil
}

func (n *Node) Subscribe(ctx context.Context) error {
	headers := make(chan *types.Header)
	sub, err := n.client.SubscribeNewHead(ctx, headers)
	if err != nil {
		n.logger.WithError(err).Error("SubscribeNewHead()")
		return err
	}

	for {
		select {
		case <-ctx.Done():
			n.logger.Info("stop subscribe")
			return nil
		case err := <-sub.Err():
			return err
		case <-headers:
			if err := n.Sync(ctx); err != nil {
				n.logger.WithError(err).Error("sync error")
			}
		}
	}
}
