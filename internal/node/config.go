package node

import (
	"errors"
	"strings"
)

type Config struct {
	URL             string `env:"ETH_NODE_URL,notEmpty"`
	CountSyncBlocks uint64 `env:"BLOCKS_SYNC_COUNT,notEmpty"`
	RateLimit       int    `env:"SYNC_RATE_LIMIT,notEmpty"`
}

func (c *Config) Validate() error {
	if !strings.HasPrefix(c.URL, "wss://") {
		return errors.New("ETH_NODE_URL is not wss schema")
	}
	return nil
}
