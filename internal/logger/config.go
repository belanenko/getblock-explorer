package logger

import (
	"errors"
	"strings"
)

type Config struct {
	LogLevel string `env:"LOG_LEVEL"`
}

func (c *Config) Validate() error {
	if c.LogLevel == "" {
		return errors.New("log level is empty")
	}
	switch strings.ToLower(c.LogLevel) {
	case "debug":
	case "info":
	case "error":
	default:
		return errors.New("incorrect log level")
	}
	return nil
}
