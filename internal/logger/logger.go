package logger

import "github.com/sirupsen/logrus"

type Logger struct {
	*logrus.Logger
}

func New(cfg *Config) (*Logger, error) {
	l := logrus.New()
	l.SetFormatter(&logrus.JSONFormatter{})
	l.SetReportCaller(true)

	log := &Logger{l}
	if err := log.setLogLevel(cfg.LogLevel); err != nil {
		return nil, err
	}

	return log, nil
}

func (l *Logger) setLogLevel(lvl string) error {
	lrLvl, err := logrus.ParseLevel(lvl)
	if err != nil {
		l.Logger.WithError(err).Error("set log level")
		return err
	}
	l.SetLevel(lrLvl)
	return nil
}
