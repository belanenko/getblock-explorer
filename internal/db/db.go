package db

import (
	"context"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/pkg/errors"
	"gitlab.com/belanenko/getblock-explorer/internal/logger"
)

type DB struct {
	pool   *pgxpool.Pool
	logger *logger.Logger
}

func New(cfg *Config, l *logger.Logger) (*DB, error) {
	pool, err := pgxpool.New(context.Background(), cfg.String())
	if err != nil {
		return nil, errors.Wrap(err, "can't create new pool")
	}

	db := &DB{pool: pool, logger: l}
	return db, nil
}

func (d *DB) AddBlocks(ctx context.Context, blocks ...*types.Block) error {
	sb := strings.Builder{}
	sb.WriteString(`INSERT INTO gb.transactions (block_id, tr_hash, from_address, to_address, amount) VALUES `)

	for _, b := range blocks {
		for i, t := range b.Transactions() {
			if i != 0 {
				sb.WriteString(", ")
			}
			formAddress, err := getFromAddress(ctx, t)
			if err != nil {
				return err
			}
			toAddress := ""
			if t.To() != nil {
				toAddress = t.To().String()
			}
			v := fmt.Sprintf(`(%d,'%s','%s','%s','%s')`, b.Number().Int64(), t.Hash().String(), formAddress, toAddress, t.Value().String())
			sb.WriteString(v)
		}
	}

	sb.WriteString(";")

	_, err := d.pool.Exec(ctx, sb.String())
	if err != nil {
		d.logger.WithError(err).Error("insert blocks")
		return errors.Wrap(err, "can't insert blocks to table")
	}

	return nil
}

var timeout = time.Microsecond * 500

func getFromAddress(ctx context.Context, t *types.Transaction) (string, error) {
	msg, err := t.AsMessage(types.LatestSignerForChainID(t.ChainId()), big.NewInt(1))
	if err != nil {
	Loop:
		for {
			select {
			case <-ctx.Done():
				return "", context.Canceled
			default:
				msg, err = t.AsMessage(types.LatestSignerForChainID(t.ChainId()), big.NewInt(1))
				if err != nil {
					time.Sleep(timeout)
					continue
				}
				break Loop
			}
		}
	}
	return msg.From().String(), err
}

func (d *DB) BlockNumber(ctx context.Context) (uint64, error) {
	var blockID uint64
	if err := d.pool.QueryRow(ctx, `SELECT COALESCE(MAX(block_id),0) FROM gb.transactions;`).Scan(&blockID); err != nil {
		d.logger.WithError(err).Error("get block number")
		return 0, err
	}
	return blockID, nil
}

func (d *DB) GetAddressWithMaxBalanceChange(ctx context.Context) (string, error) {
	var address string
	if err := d.pool.QueryRow(ctx, `select address from gb.address_with_max_balance_change_for_last_100_blocks;`).Scan(&address); err != nil {
		d.logger.WithError(err).Error("GetAddressWithMaxBalanceChange()")
		return "", err
	}
	return address, nil
}
