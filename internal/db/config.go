package db

import (
	"errors"
	"fmt"
)

type Config struct {
	User     string `env:"DB_USER,notEmpty"`
	Password string `env:"DB_PASSWORD,notEmpty"`
	Host     string `env:"DB_HOST,notEmpty"`
	Port     int    `env:"DB_PORT,notEmpty"`
}

func (c *Config) String() string {
	return fmt.Sprintf(`postgresql://%s:%s@%s:%d/postgres?sslmode=disable`, c.User, c.Password, c.Host, c.Port)
}

func (c *Config) Validate() error {
	if c.Port < 1 || c.Port > 65535 {
		return errors.New("incorrect port value")
	}
	return nil
}
