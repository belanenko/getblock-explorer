package api

import "errors"

type Config struct {
	Port int `env:"EXPLORER_PORT,notEmpty"`
}

func (c *Config) Validate() error {
	if c.Port < 1 || c.Port > 65535 {
		return errors.New("incorrect port value")
	}
	return nil
}
