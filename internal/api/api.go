package api

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/belanenko/getblock-explorer/internal/db"
	"gitlab.com/belanenko/getblock-explorer/internal/logger"
)

type API struct {
	cfg *Config
	db  *db.DB
	log *logger.Logger
}

func New(cfg *Config, d *db.DB, log *logger.Logger) *API {
	return &API{
		cfg: cfg,
		db:  d,
		log: log,
	}
}

func (a *API) Start(ctx context.Context) error {
	r := gin.Default()
	srv := http.Server{
		Addr:         fmt.Sprintf(":%d", a.cfg.Port),
		Handler:      r,
		ReadTimeout:  time.Second,
		WriteTimeout: time.Second,
	}

	r.GET("/address", func(c *gin.Context) {
		address, err := a.db.GetAddressWithMaxBalanceChange(c)
		if err != nil {
			if err := c.Error(err); err != nil {
				a.log.WithError(err).Error("c.Error()")
			}
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"address": address,
		})
	})

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			if err != nil {
				if errors.Is(err, context.Canceled) {
					return
				}
				a.log.WithError(err).Error("listen and serve")
			}
		}
	}()

	<-ctx.Done()
	a.log.Info("server shutdown...")
	return srv.Shutdown(ctx)
}
