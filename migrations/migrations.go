package migrations

import (
	"embed"

	"github.com/pressly/goose/v3"
	"gitlab.com/belanenko/getblock-explorer/internal/logger"
)

//go:embed sql/*.sql
var embedMigrations embed.FS

type Migrations struct {
	logger           *logger.Logger
	connectionString string
}

func New(connectionString string, log *logger.Logger) *Migrations {
	return &Migrations{
		connectionString: connectionString,
		logger:           log,
	}
}

func (m *Migrations) Up() error {
	goose.SetBaseFS(embedMigrations)
	if err := goose.SetDialect("postgres"); err != nil {
		m.logger.WithError(err).Error("set dialect")
		return err
	}

	db, err := goose.OpenDBWithDriver("postgres", m.connectionString)
	if err != nil {
		m.logger.WithError(err).Error("OpenDBWithDriver()")
		return err
	}

	if err := goose.Up(db, "sql"); err != nil {
		m.logger.WithError(err).Error("Up()")
		return err
	}

	return nil
}
