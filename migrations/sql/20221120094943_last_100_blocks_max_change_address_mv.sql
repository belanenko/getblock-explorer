-- +goose Up
-- +goose StatementBegin
CREATE VIEW gb.address_with_max_balance_change_for_last_100_blocks AS  WITH max_block_id AS (
         SELECT max(transactions.block_id) AS max_block_id
           FROM gb.transactions
        ), transactions_with_limit AS (
         SELECT max_block_id.max_block_id,
            tr.id,
            tr.block_id,
            tr.tr_hash,
            tr.from_address,
            tr.to_address,
            tr.amount,
            tr.created_at
           FROM max_block_id
             JOIN gb.transactions tr ON true
          WHERE tr.block_id > (max_block_id.max_block_id - 100)
        ), sends AS (
         SELECT transactions_with_limit.from_address,
            - sum(transactions_with_limit.amount::numeric) AS balance
           FROM transactions_with_limit
          GROUP BY transactions_with_limit.from_address
        ), recived AS (
         SELECT transactions_with_limit.to_address,
            sum(transactions_with_limit.amount::numeric) AS balance
           FROM transactions_with_limit
          GROUP BY transactions_with_limit.to_address
        ), total AS (
         SELECT sends.from_address AS address,
            sends.balance
           FROM sends
        UNION
         SELECT recived.to_address AS address,
            recived.balance
           FROM recived
        )
 SELECT total.address,
    abs(sum(total.balance)) AS change_value
   FROM total
  GROUP BY total.address
  ORDER BY (abs(sum(total.balance))) DESC
 LIMIT 1;

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
