-- +goose Up
-- +goose StatementBegin
CREATE TABLE gb.transactions (
    id SERIAL PRIMARY KEY,
    block_id BIGINT NOT NULL,
    tr_hash TEXT NOT NULL,
    from_address TEXT,
    to_address TEXT,
    amount TEXT,
    created_at TIMESTAMPTZ DEFAULT now()
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE gb.transactions;
-- +goose StatementEnd
